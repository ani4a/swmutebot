import argparse

def Args():
    parser = argparse.ArgumentParser(description="args")
    parser.add_argument('-id', dest="api_id", default=2040, type=int)
    parser.add_argument('-hash', dest="api_hash", default='b18441a1ff607e10a989891a5462e627', type=str)
    parser.add_argument('-key', dest="api_key", type=str)
    parser.add_argument('-adm', dest="start_admin", default=None, type=int)
    return(parser.parse_args())