from bot.utils.telethonmod import username


def main(*args):
    return '|'.join([i for i in [
        rf'^\/({i})+(\@{username}\w*(_\w+)*)?([ \f\n\r\t\v\u00A0\u2028\u2029].*)?$' for i in args
    ]])
