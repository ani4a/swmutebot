from bot.db import *


async def purgepromote(event, user, sender):
    admincheck = sadms.get_or_none(id=sender)
    if admincheck is None:  
        await event.reply("Ты не админ!")
    else:
        textObj, admincheckmsg = purgemasters.get_or_create(id=user)
        if admincheckmsg is True:
            await event.reply("Назначен новый админ!")
        else:
            await event.reply("Он уже админ!")

async def sadmpromote(event, user, sender):
    admincheck = sadms.get_or_none(id=sender)
    if admincheck is None:  
        await event.reply("Ты не админ!")
    else:
        textObj, admincheckmsg = sadms.get_or_create(id=user)
        if admincheckmsg is True:
            await event.reply("Назначен новый <strong>супер</strong> админ!")
        else:
            await event.reply("Он уже <strong>супер</strong> админ!")