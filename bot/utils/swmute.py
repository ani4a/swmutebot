from bot import data


async def getsw(event, chat_id, swmuteuser, adminuid):
    if not chat_id in data.swmuted:
        data.swmuted[chat_id] = set()

    if swmuteuser in data.swmuted[chat_id]:
        data.swmuted[chat_id].remove(swmuteuser)
        if adminuid == 328508967:
            return await event.reply("<strong>Вывожу мусоров с хаты чела!</strong>")
        else:
            return await event.reply("<strong>Снял свмут!</strong>")

    else:
        data.swmuted[chat_id].add(swmuteuser)
        if adminuid == 328508967:
            await event.reply("<strong>Сватнул чела</strong>")
        else:
            await event.reply("<strong>Свмутнул чела</strong>")


async def swall(event, chat_id, adminuid):
    swmuteuser = 1337

    if not chat_id in data.swmuted:
        data.swmuted[chat_id] = set()

    if swmuteuser in data.swmuted[chat_id]:
        data.swmuted[chat_id].remove(swmuteuser)
        if adminuid == 328508967:
            return await event.reply("<strong>Вывожу мусоров с хат!</strong>")
        else:
            return await event.reply("<strong>Снял свмут всем!</strong>")

    else:
        data.swmuted[chat_id].add(swmuteuser)
        if adminuid == 328508967:
            await event.reply("<strong>Сватнул всех</strong>")
        else:
            await event.reply("<strong>Свмутнул челов</strong>")
