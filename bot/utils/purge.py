from bot.utils import *
import shlex
from telethon.tl.types import User



def get_args(message):
	try:
		message = message.raw_text
	except AttributeError:
		pass
	if not message:
		return False
	message = message.split(maxsplit=1)
	if len(message) <= 1:
		return []
	message = message[1]
	try:
		split = shlex.split(message)
	except ValueError:
		return message  # Cannot split, let's assume that it's just one long message
	return list(filter(lambda x: len(x) > 0, split))

async def main(event):
	if not event.is_reply:
		await event.reply("<strong> Откуда мне начать пургэ?) </strong>")
		return
	from_users = set()
	args = get_args(event)
	for arg in args:
		try:
			try:
				arg = int(arg)
			except:
				pass
			entity = await event.client.get_entity(arg)
			if isinstance(entity, User):
				from_users.add(entity.id)
		except ValueError:
			pass
	msgs = []
	from_ids = set()
	if await event.client.is_bot():
		if not event.is_channel:
			await event.reply("Пург можно только в супергруппах!")
			return
		for msg in range(event.reply_to_msg_id, event.id + 1):
			msgs.append(msg)
			if len(msgs) >= 99:
				try:
					await event.client.delete_messages(event.to_id, msgs)
				except errors.rpcerrorlist.MessageDeleteForbiddenError:
					pass
				msgs.clear()
	else:
		async for msg in event.client.iter_messages(
				entity=event.to_id,
				min_id=event.reply_to_msg_id - 1,
				reverse=True):
			if from_users and msg.sender_id not in from_users:
				continue
			msgs.append(msg.id)
			from_ids.add(msg.sender_id)
			if len(msgs) >= 99:
				try:
					await event.client.delete_messages(event.to_id, msgs)
				except errors.rpcerrorlist.MessageDeleteForbiddenError:
					pass
				msgs.clear()
	if msgs:
		try:
			await event.client.delete_messages(event.to_id, msgs)
		except errors.rpcerrorlist.MessageDeleteForbiddenError:
			pass