from peewee import Model, IntegerField, SqliteDatabase


db = SqliteDatabase('sqlite.db')

class sadms(Model):
    id = IntegerField()

    class Meta:
        database = db

class purgemasters(Model):
    id = IntegerField()

    class Meta:
        database = db

class swmutes(Model):
    id = IntegerField()
    who = IntegerField()

    class Meta:
        database = db