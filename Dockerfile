FROM python:3.8-slim
WORKDIR /code
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY bot/ .
CMD [ "python", "./__main__.py" ]